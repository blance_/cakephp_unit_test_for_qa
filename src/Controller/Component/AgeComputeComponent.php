<?php
namespace App\Controller\Component;

use Cake\Controller\Component;
use Cake\Controller\ComponentRegistry;

/**
 * AgeCompute component
 */
class AgeComputeComponent extends Component
{
    /**
     * Default configuration.
     *
     * @var array
     */
    protected $_defaultConfig = [];

    /**
     * computAge method
     * This function will generate available times from rsource time tabls
     * @param int $minTreatmentMinutes minimum treatment minutes
     * @return int $age
     */
    public function computeAge(
        string $dateTime
    ) {
        $from = date_create($dateTime);
        $to   = date_create('today');
        return $from->diff($to)->y;
    }
}
